


import {UrlTester} from "./utils/UrlTester";
import {PrerenderService} from "./utils/PrerenderService";
import {AjaxUrls} from "./utils/AjaxUrls";
const url = require("url");

export interface AJAXToolsOptions {
  passThruLinks: Array<string>;
  useS3: boolean;
  customS3?: string;

  useDynamo: boolean;
  customDynamo?: string;

  key: string;
  secret_key: string;
}

export class AJAXTools {

  public static shouldServePrerenderPage(req, options: AJAXToolsOptions) {
    return UrlTester.shouldServePrerenderedPage(req, options.passThruLinks);
  }

  public static getPrerenderPage(req, res, options: AJAXToolsOptions): Promise<string> {
    const full_url: string = AjaxUrls.getAjaxUrl(req);

    const parsedUrl = url.parse(full_url);
    const domain: string = parsedUrl.hostname;
    const pathquery: string = (parsedUrl.hash) ? parsedUrl.hash : parsedUrl.path;


    if (options.useS3) {
      if (options.customS3) {
        throw new Error("Not supported yet");
      }
      else {
        return PrerenderService.getS3Prerender(domain, pathquery, options);
      }
    }

    else {
      if (options.customDynamo) {
        throw new Error("Not supported yet.");
      }
      else {
        return PrerenderService.getDynamoPrerender(domain, pathquery);
      }
    }
  }




}
import chai = require('chai');
const should = chai.should();

import * as TypeMoq from "typemoq";
import {AjaxUrls} from "../../utils/AjaxUrls";

describe('AjaxUrls', function() {

  it('AjaxUrls handles escape fragment', function () {
    let req = {
      headers: {
        "user-agent": "Chrome",
        "host": "www.testsite.com"
      },
      method: 'GET',
      url: '/?_escaped_fragment_=/some/page',
      protocol: 'http'
    };

    AjaxUrls.getAjaxUrl(req).should.equal("http://www.testsite.com/#!/some/page");
  });

})
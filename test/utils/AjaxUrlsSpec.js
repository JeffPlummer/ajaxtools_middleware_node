"use strict";
var chai = require("chai");
var should = chai.should();
var AjaxUrls_1 = require("../../utils/AjaxUrls");
describe('AjaxUrls', function () {
    it('AjaxUrls handles escape fragment', function () {
        var req = {
            headers: {
                "user-agent": "Chrome",
                "host": "www.testsite.com"
            },
            method: 'GET',
            url: '/?_escaped_fragment_=/some/page',
            protocol: 'http'
        };
        AjaxUrls_1.AjaxUrls.getAjaxUrl(req).should.equal("http://www.testsite.com/#!/some/page");
    });
});

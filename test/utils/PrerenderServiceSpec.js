"use strict";
var chai = require("chai");
var should = chai.should();
var PrerenderService_1 = require("../../utils/PrerenderService");
var AWS = require('mock-aws');
var AWS2 = require('aws-sdk-mock');
describe('PrerenderService', function () {
    it('getS3Prerender', function (done) {
        AWS.mock('S3', 'getObject', { Body: "Test" });
        PrerenderService_1.PrerenderService.getS3Prerender("FakeDomain", "FakePath")
            .then(function (body) {
            body.should.equal("Test");
            AWS.restore();
            done();
        });
    });
    it('getDynamoPrerender', function (done) {
        AWS2.mock('DynamoDB.DocumentClient', 'get', function (params, callback) {
            callback(null, { Item: { page: "Test" } });
        });
        PrerenderService_1.PrerenderService.getDynamoPrerender("FakeDomain", "FakePath")
            .then(function (body) {
            body.should.equal("Test");
            done();
        });
    });
});

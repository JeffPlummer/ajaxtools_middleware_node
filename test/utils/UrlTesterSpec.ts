import chai = require('chai');
const should = chai.should();

import * as TypeMoq from "typemoq";
import {UrlTester} from "../../utils/UrlTester";

describe('UrlTester', function() {

  it('Normal user using chrome should not serve prerendered page', function () {
    let req = {
      headers: {
        "user-agent": "Chrome"
      },
      method: 'GET',
      url: 'http://www.testsite.com/foobar?foo=bar'
    };

    UrlTester.shouldServePrerenderedPage(req, []).should.equal(false);
  });


  it('Googlebot should serve prerendered page', function () {
    let req = {
      headers: {
        "user-agent": "googlebot"
      },
      method: 'GET',
      url: 'http://www.testsite.com/foobar?foo=bar'
    };

    UrlTester.shouldServePrerenderedPage(req, []).should.equal(true);
  });

  it('Escape Fragment should serve prerendered page', function () {
    let req = {
      headers: {
        "user-agent": "chrom"
      },
      method: 'GET',
      url: 'http://www.testsite.com/foobar?_escaped_fragment_=bar'
    };

    UrlTester.shouldServePrerenderedPage(req, []).should.equal(true);
  });

  it('Buffer agent should serve prerendered page', function () {
    let req = {
      headers: {
        "user-agent": "chrom",
        "x-bufferbot": "whatever"
      },
      method: 'GET',
      url: 'http://www.testsite.com/foobar'
    };

    UrlTester.shouldServePrerenderedPage(req, []).should.equal(true);
  });

  it('No user agent should Pass thru page', function () {
    let req = {
      headers: {
      },
      method: 'GET',
      url: 'http://www.testsite.com/foobar?foo=bar'
    };

    UrlTester.shouldServePrerenderedPage(req, []).should.equal(false);
  });

  it('Googlebot should still get resources directly', function () {
    let req = {
      headers: {
        "user-agent": "googlebot"
      },
      method: 'GET',
      url: 'http://www.testsite.com/foobar.js'
    };

    UrlTester.shouldServePrerenderedPage(req, []).should.equal(false);
  });

  it('Googlebot should still get passthru resources directly', function () {
    let req = {
      headers: {
        "user-agent": "googlebot"
      },
      method: 'GET',
      url: 'http://www.testsite.com/foobar'
    };

    UrlTester.shouldServePrerenderedPage(req, ['foobar']).should.equal(false);
  });

  it('Googlebot should still get passthru resources directly', function () {
    let req = {
      headers: {
        "user-agent": "chrome"
      },
      method: 'PUT',
      url: 'http://www.testsite.com/foobar'
    };

    UrlTester.shouldServePrerenderedPage(req, ['foobar']).should.equal(false);
  });

});
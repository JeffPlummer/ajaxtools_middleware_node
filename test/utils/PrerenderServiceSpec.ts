import chai = require('chai');
const should = chai.should();

import {PrerenderService} from "../../utils/PrerenderService";
var AWS = require('mock-aws');
var AWS2 = require('aws-sdk-mock');

describe('PrerenderService', function() {

  it('getS3Prerender', function (done) {

    AWS.mock('S3', 'getObject', {Body: "Test"});

    PrerenderService.getS3Prerender("FakeDomain", "FakePath")
      .then( (body) => {
        body.should.equal("Test");
        AWS.restore();
        done();
      })

  });


  it('getDynamoPrerender', function (done) {

    AWS2.mock('DynamoDB.DocumentClient', 'get',  (params, callback) => {
          callback(null, {Item: { page: "Test"} })
    });

    PrerenderService.getDynamoPrerender("FakeDomain", "FakePath")
      .then( (body) => {
        body.should.equal("Test");
        done();
      })

  });

});
"use strict";
var UrlTester_1 = require("./utils/UrlTester");
var PrerenderService_1 = require("./utils/PrerenderService");
var AjaxUrls_1 = require("./utils/AjaxUrls");
var url = require("url");
var AJAXTools = (function () {
    function AJAXTools() {
    }
    AJAXTools.shouldServePrerenderPage = function (req, options) {
        return UrlTester_1.UrlTester.shouldServePrerenderedPage(req, options.passThruLinks);
    };
    AJAXTools.getPrerenderPage = function (req, res, options) {
        var full_url = AjaxUrls_1.AjaxUrls.getAjaxUrl(req);
        var parsedUrl = url.parse(full_url);
        var domain = parsedUrl.hostname;
        var pathquery = (parsedUrl.hash) ? parsedUrl.hash : parsedUrl.path;
        if (options.useS3) {
            if (options.customS3) {
                throw new Error("Not supported yet");
            }
            else {
                return PrerenderService_1.PrerenderService.getS3Prerender(domain, pathquery, options);
            }
        }
        else {
            if (options.customDynamo) {
                throw new Error("Not supported yet.");
            }
            else {
                return PrerenderService_1.PrerenderService.getDynamoPrerender(domain, pathquery);
            }
        }
    };
    return AJAXTools;
}());
exports.AJAXTools = AJAXTools;

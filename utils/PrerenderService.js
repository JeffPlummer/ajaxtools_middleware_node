"use strict";
var AWS = require('aws-sdk');
var urlParser = require("url");
var PrerenderService = (function () {
    function PrerenderService() {
    }
    PrerenderService.getS3Prerender = function (domain, url, options) {
        return new Promise(function (resolve, reject) {
            AWS.config.region = 'us-east-2';
            var s3bucket = new AWS.S3({
                accessKeyId: options.key,
                secretAccessKey: options.secret_key,
                params: { Bucket: "domain-prerenders"
                }
            });
            var urlkey = PrerenderService.getUrlKey(url);
            var params = { Key: domain + '/' + urlkey };
            s3bucket.getObject(params, function (err, data) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(data.Body.toString());
                }
            });
        });
    };
    PrerenderService.getDynamoPrerender = function (domain, url) {
        var urlKey = PrerenderService.getUrlKey(url);
        return new Promise(function (resolve, reject) {
            var params = {
                TableName: "domain-prerenders",
                Key: {
                    domain: domain,
                    url: urlKey
                }
            };
            var docClient = new AWS.DynamoDB.DocumentClient();
            docClient.get(params, function (err, data) {
                if (err) {
                    reject({ domain: domain, message: "getPrerender failed for url: " + url, err: err });
                }
                else {
                    if (!data.Item) {
                        resolve();
                    }
                    else {
                        var result = data.Item.page;
                        resolve(result);
                    }
                }
            });
        });
    };
    PrerenderService.getUrlKey = function (url) {
        // Strip last '/' if link ends in '/'
        var preppedUrl = ((url !== '/') && (url.slice(-1) === '/')) ? url.slice(0, -1) : url;
        var parsedUrl = urlParser.parse(preppedUrl);
        var urlkey = null;
        if ((!parsedUrl.path) || (parsedUrl.hash)) {
            urlkey = parsedUrl.hash.replace(/\//g, '_');
        }
        else {
            urlkey = parsedUrl.path.replace(/\//g, '_');
        }
        return urlkey;
    };
    ;
    return PrerenderService;
}());
exports.PrerenderService = PrerenderService;

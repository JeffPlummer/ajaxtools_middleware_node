const url = require("url");

export class UrlTester {


  private static crawlerUserAgents = [
    'googlebot',
    'yahoo',
    'bingbot',
    'baiduspider',
    'facebookexternalhit',
    'twitterbot',
    'rogerbot',
    'linkedinbot',
    'embedly',
    'quora link preview',
    'showyoubot',
    'outbrain',
    'pinterest/0.',
    'developers.google.com/+/web/snippet',
    'slackbot',
    'vkShare',
    'W3C_Validator',
    'redditbot',
    'Applebot',
    'WhatsApp',
    'flipboard',
    'tumblr',
    'bitlybot',
    'SkypeUriPreview',
    'nuzzel',
    'Discordbot',
    'Google Page Speed'
  ];

  private static extensionsToIgnore = [
    '.js',
    '.map',
    '.css',
    '.xml',
    '.less',
    '.png',
    '.jpg',
    '.jpeg',
    '.gif',
    '.pdf',
    '.doc',
    '.txt',
    '.ico',
    '.rss',
    '.zip',
    '.mp3',
    '.rar',
    '.exe',
    '.wmv',
    '.doc',
    '.avi',
    '.ppt',
    '.mpg',
    '.mpeg',
    '.tif',
    '.wav',
    '.mov',
    '.psd',
    '.ai',
    '.xls',
    '.mp4',
    '.m4a',
    '.swf',
    '.dat',
    '.dmg',
    '.iso',
    '.flv',
    '.m4v',
    '.torrent',
    '.woff',
    '.ttf',
    '.svg'
  ];


  public static shouldServePrerenderedPage(req, passThruList: Array<string>): boolean {
    const userAgent = req.headers['user-agent'];
    const bufferAgent = req.headers['x-bufferbot'];

    //False Cases
    if (!userAgent) return false;
    if (req.method != 'GET' && req.method != 'HEAD') return false;

    //Resources
    if(UrlTester.ignoreExtension(req.url)) return false;

    //Whitelist
    if(UrlTester.shouldPassThru(req.url, passThruList)) return false;

    //if it contains _escaped_fragment_, show prerendered page
    const parsedQuery = url.parse(req.url, true).query;
    if(parsedQuery && parsedQuery['_escaped_fragment_'] !== undefined) return true;

    //if it is a bot...show prerendered page
    if (UrlTester.isUserAgentABot(userAgent)) return true;

    //if it is BufferBot...show prerendered page
    if(bufferAgent) return true;


    //Otherwise
    return false;
  }

  private static ignoreExtension(url: string): boolean {
    return UrlTester.extensionsToIgnore.some((extension) => {
      return url.toLowerCase().indexOf(extension) !== -1;
    });
  }


  private static isUserAgentABot(userAgent: string): boolean {
    return UrlTester.crawlerUserAgents.some((crawlerUserAgent) => {
      return userAgent.toLowerCase().indexOf(crawlerUserAgent.toLowerCase()) !== -1;
    });
  }

  private static shouldPassThru(url: string, passThruList: Array<string>) {
    if ( (passThruList) && (passThruList.length) ) {
      return passThruList.every((pass) => {
        return (new RegExp(pass)).test(url) ;
      });
    }
    else {
      return false;
    }

  }

}
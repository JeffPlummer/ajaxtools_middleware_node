import {AJAXToolsOptions} from "../AJAXTools";
const AWS = require('aws-sdk');
const urlParser = require("url");



export class PrerenderService {
  public static getS3Prerender(domain: string, url: string, options: AJAXToolsOptions): Promise<string> {
    return new Promise<string>(function (resolve, reject) {
      AWS.config.region = 'us-east-2';
      const s3bucket = new AWS.S3(
        {
          accessKeyId: options.key,
          secretAccessKey: options.secret_key,
          params: {Bucket: "domain-prerenders"
          }
        });
      const urlkey = PrerenderService.getUrlKey(url);

      const params = {Key: domain + '/' + urlkey};
      s3bucket.getObject(params, function (err, data) {
        if (err) {
          reject(err);
        } else {
          resolve(data.Body.toString());
        }
      });
    });
  }

  public static getDynamoPrerender(domain: string, url: string): Promise<string> {
    const urlKey: string = PrerenderService.getUrlKey(url);
    return new Promise<string>(function (resolve, reject) {
      const params = {
        TableName: "domain-prerenders",
        Key: {
          domain: domain,
          url: urlKey
        }
      };

      const docClient = new AWS.DynamoDB.DocumentClient();
      docClient.get(params, function (err: Error, data: any) {
        if (err) {
          reject({domain: domain, message: "getPrerender failed for url: " + url, err: err});
        }
        else {
          if (!data.Item) {
            resolve();
          }
          else {
            const result: string = data.Item.page;
            resolve(result);
          }
        }
      });
    });
  }


  private static getUrlKey(url): string {
    // Strip last '/' if link ends in '/'
    const preppedUrl = ( (url !== '/') && (url.slice(-1) === '/') ) ? url.slice(0, -1) : url;
    const parsedUrl = urlParser.parse(preppedUrl);

    let urlkey = null;
    if ((!parsedUrl.path) || (parsedUrl.hash)) {
      urlkey = parsedUrl.hash.replace(/\//g, '_');
    }
    else {
      urlkey = parsedUrl.path.replace(/\//g, '_');
    }
    return urlkey;
  };
}
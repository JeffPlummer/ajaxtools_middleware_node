"use strict";
var url = require("url");
var UrlTester = (function () {
    function UrlTester() {
    }
    UrlTester.shouldServePrerenderedPage = function (req, passThruList) {
        var userAgent = req.headers['user-agent'];
        var bufferAgent = req.headers['x-bufferbot'];
        //False Cases
        if (!userAgent)
            return false;
        if (req.method != 'GET' && req.method != 'HEAD')
            return false;
        //Resources
        if (UrlTester.ignoreExtension(req.url))
            return false;
        //Whitelist
        if (UrlTester.shouldPassThru(req.url, passThruList))
            return false;
        //if it contains _escaped_fragment_, show prerendered page
        var parsedQuery = url.parse(req.url, true).query;
        if (parsedQuery && parsedQuery['_escaped_fragment_'] !== undefined)
            return true;
        //if it is a bot...show prerendered page
        if (UrlTester.isUserAgentABot(userAgent))
            return true;
        //if it is BufferBot...show prerendered page
        if (bufferAgent)
            return true;
        //Otherwise
        return false;
    };
    UrlTester.ignoreExtension = function (url) {
        return UrlTester.extensionsToIgnore.some(function (extension) {
            return url.toLowerCase().indexOf(extension) !== -1;
        });
    };
    UrlTester.isUserAgentABot = function (userAgent) {
        return UrlTester.crawlerUserAgents.some(function (crawlerUserAgent) {
            return userAgent.toLowerCase().indexOf(crawlerUserAgent.toLowerCase()) !== -1;
        });
    };
    UrlTester.shouldPassThru = function (url, passThruList) {
        if ((passThruList) && (passThruList.length)) {
            return passThruList.every(function (pass) {
                return (new RegExp(pass)).test(url);
            });
        }
        else {
            return false;
        }
    };
    return UrlTester;
}());
UrlTester.crawlerUserAgents = [
    // 'googlebot',
    'yahoo',
    'bingbot',
    'baiduspider',
    'facebookexternalhit',
    'twitterbot',
    'rogerbot',
    'linkedinbot',
    'embedly',
    'quora link preview',
    'showyoubot',
    'outbrain',
    'pinterest/0.',
    'developers.google.com/+/web/snippet',
    'slackbot',
    'vkShare',
    'W3C_Validator',
    'redditbot',
    'Applebot',
    'WhatsApp',
    'flipboard',
    'tumblr',
    'bitlybot',
    'SkypeUriPreview',
    'nuzzel',
    'Discordbot',
    'Google Page Speed'
];
UrlTester.extensionsToIgnore = [
    '.js',
    '.map',
    '.css',
    '.xml',
    '.less',
    '.png',
    '.jpg',
    '.jpeg',
    '.gif',
    '.pdf',
    '.doc',
    '.txt',
    '.ico',
    '.rss',
    '.zip',
    '.mp3',
    '.rar',
    '.exe',
    '.wmv',
    '.doc',
    '.avi',
    '.ppt',
    '.mpg',
    '.mpeg',
    '.tif',
    '.wav',
    '.mov',
    '.psd',
    '.ai',
    '.xls',
    '.mp4',
    '.m4a',
    '.swf',
    '.dat',
    '.dmg',
    '.iso',
    '.flv',
    '.m4v',
    '.torrent',
    '.woff',
    '.ttf',
    '.svg'
];
exports.UrlTester = UrlTester;


export class AjaxUrls {

  // Get the URL to the AJAX version of this page
  public static getAjaxUrl(req): string {
    const full_url: string = req.protocol + '://' + req.headers.host + req.url;
    const urlParts = req.url.split('?_escaped_fragment_=');

    // If no fragment in URL this is not a request for an HTML snapshot
    // of an AJAX page.
    if ( urlParts.length !== 2 ) return full_url;

    // var url = req.url;
    let translatedUrl: string;

    //If contains, "_escaped_fragment_", need to convert back to #! link
    if(urlParts.length === 2) {
      const protocol = req.protocol;
      translatedUrl = protocol + '://' + req.headers.host;
      var path = urlParts[0];
      var fragment = urlParts[1];
      translatedUrl += path + '#!' + decodeURIComponent(fragment);
    }


    return translatedUrl;
  }

}
"use strict";
var AjaxUrls = (function () {
    function AjaxUrls() {
    }
    // Get the URL to the AJAX version of this page
    AjaxUrls.getAjaxUrl = function (req) {
        var full_url = req.protocol + '://' + req.headers.host + req.url;
        var urlParts = req.url.split('?_escaped_fragment_=');
        // If no fragment in URL this is not a request for an HTML snapshot
        // of an AJAX page.
        if (urlParts.length !== 2)
            return full_url;
        // var url = req.url;
        var translatedUrl;
        //If contains, "_escaped_fragment_", need to convert back to #! link
        if (urlParts.length === 2) {
            var protocol = req.protocol;
            translatedUrl = protocol + '://' + req.headers.host;
            var path = urlParts[0];
            var fragment = urlParts[1];
            translatedUrl += path + '#!' + decodeURIComponent(fragment);
        }
        return translatedUrl;
    };
    return AjaxUrls;
}());
exports.AjaxUrls = AjaxUrls;

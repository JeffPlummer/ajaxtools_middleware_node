var AJAXTools = require("./AJAXTools");

module.exports = function(options) {

  return function(req, res, next) {

    if (!AJAXTools.AJAXTools.shouldServePrerenderPage(req, options)) {
      next(); //Shouldn't serve prerendered page, next to serve up the real page
    }
    else {
      AJAXTools.AJAXTools.getPrerenderPage(req, res, options)
        .then( function (pagebody) {
          res.set('Content-Type', 'text/html');
          res.send(pagebody);
        })
        .catch( function(err) {
          next();  //We failed, next to serve up the real page.
        });
    }
  }
};
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("url"), require("aws-sdk"));
	else if(typeof define === 'function' && define.amd)
		define(["url", "aws-sdk"], factory);
	else if(typeof exports === 'object')
		exports["ajaxtools-express"] = factory(require("url"), require("aws-sdk"));
	else
		root["ajaxtools-express"] = factory(root["url"], root["aws-sdk"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_5__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.l = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };

/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};

/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};

/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("url");

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var UrlTester_1 = __webpack_require__(4);
var PrerenderService_1 = __webpack_require__(3);
var AjaxUrls_1 = __webpack_require__(2);
var url = __webpack_require__(0);
var AJAXTools = (function () {
    function AJAXTools() {
    }
    AJAXTools.shouldServePrerenderPage = function (req, options) {
        return UrlTester_1.UrlTester.shouldServePrerenderedPage(req, options.passThruLinks);
    };
    AJAXTools.getPrerenderPage = function (req, res, options) {
        var full_url = AjaxUrls_1.AjaxUrls.getAjaxUrl(req);
        var parsedUrl = url.parse(full_url);
        var domain = parsedUrl.hostname;
        var pathquery = (parsedUrl.hash) ? parsedUrl.hash : parsedUrl.path;
        if (options.useS3) {
            if (options.customS3) {
                throw new Error("Not supported yet");
            }
            else {
                return PrerenderService_1.PrerenderService.getS3Prerender(domain, pathquery, options);
            }
        }
        else {
            if (options.customDynamo) {
                throw new Error("Not supported yet.");
            }
            else {
                return PrerenderService_1.PrerenderService.getDynamoPrerender(domain, pathquery);
            }
        }
    };
    return AJAXTools;
}());
exports.AJAXTools = AJAXTools;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var AjaxUrls = (function () {
    function AjaxUrls() {
    }
    // Get the URL to the AJAX version of this page
    AjaxUrls.getAjaxUrl = function (req) {
        var full_url = req.protocol + '://' + req.headers.host + req.url;
        var urlParts = req.url.split('?_escaped_fragment_=');
        // If no fragment in URL this is not a request for an HTML snapshot
        // of an AJAX page.
        if (urlParts.length !== 2)
            return full_url;
        // var url = req.url;
        var translatedUrl;
        //If contains, "_escaped_fragment_", need to convert back to #! link
        if (urlParts.length === 2) {
            var protocol = req.protocol;
            translatedUrl = protocol + '://' + req.headers.host;
            var path = urlParts[0];
            var fragment = urlParts[1];
            translatedUrl += path + '#!' + decodeURIComponent(fragment);
        }
        return translatedUrl;
    };
    return AjaxUrls;
}());
exports.AjaxUrls = AjaxUrls;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var AWS = __webpack_require__(5);
var urlParser = __webpack_require__(0);
var PrerenderService = (function () {
    function PrerenderService() {
    }
    PrerenderService.getS3Prerender = function (domain, url, options) {
        return new Promise(function (resolve, reject) {
            AWS.config.region = 'us-east-2';
            var s3bucket = new AWS.S3({
                accessKeyId: options.key,
                secretAccessKey: options.secret_key,
                params: { Bucket: "domain-prerenders"
                }
            });
            var urlkey = PrerenderService.getUrlKey(url);
            var params = { Key: domain + '/' + urlkey };
            s3bucket.getObject(params, function (err, data) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(data.Body.toString());
                }
            });
        });
    };
    PrerenderService.getDynamoPrerender = function (domain, url) {
        var urlKey = PrerenderService.getUrlKey(url);
        return new Promise(function (resolve, reject) {
            var params = {
                TableName: "domain-prerenders",
                Key: {
                    domain: domain,
                    url: urlKey
                }
            };
            var docClient = new AWS.DynamoDB.DocumentClient();
            docClient.get(params, function (err, data) {
                if (err) {
                    reject({ domain: domain, message: "getPrerender failed for url: " + url, err: err });
                }
                else {
                    if (!data.Item) {
                        resolve();
                    }
                    else {
                        var result = data.Item.page;
                        resolve(result);
                    }
                }
            });
        });
    };
    PrerenderService.getUrlKey = function (url) {
        // Strip last '/' if link ends in '/'
        var preppedUrl = ((url !== '/') && (url.slice(-1) === '/')) ? url.slice(0, -1) : url;
        var parsedUrl = urlParser.parse(preppedUrl);
        var urlkey = null;
        if ((!parsedUrl.path) || (parsedUrl.hash)) {
            urlkey = parsedUrl.hash.replace(/\//g, '_');
        }
        else {
            urlkey = parsedUrl.path.replace(/\//g, '_');
        }
        return urlkey;
    };
    ;
    return PrerenderService;
}());
exports.PrerenderService = PrerenderService;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var url = __webpack_require__(0);
var UrlTester = (function () {
    function UrlTester() {
    }
    UrlTester.shouldServePrerenderedPage = function (req, passThruList) {
        var userAgent = req.headers['user-agent'];
        var bufferAgent = req.headers['x-bufferbot'];
        //False Cases
        if (!userAgent)
            return false;
        if (req.method != 'GET' && req.method != 'HEAD')
            return false;
        //Resources
        if (UrlTester.ignoreExtension(req.url))
            return false;
        //Whitelist
        if (UrlTester.shouldPassThru(req.url, passThruList))
            return false;
        //if it contains _escaped_fragment_, show prerendered page
        var parsedQuery = url.parse(req.url, true).query;
        if (parsedQuery && parsedQuery['_escaped_fragment_'] !== undefined)
            return true;
        //if it is a bot...show prerendered page
        if (UrlTester.isUserAgentABot(userAgent))
            return true;
        //if it is BufferBot...show prerendered page
        if (bufferAgent)
            return true;
        //Otherwise
        return false;
    };
    UrlTester.ignoreExtension = function (url) {
        return UrlTester.extensionsToIgnore.some(function (extension) {
            return url.toLowerCase().indexOf(extension) !== -1;
        });
    };
    UrlTester.isUserAgentABot = function (userAgent) {
        return UrlTester.crawlerUserAgents.some(function (crawlerUserAgent) {
            return userAgent.toLowerCase().indexOf(crawlerUserAgent.toLowerCase()) !== -1;
        });
    };
    UrlTester.shouldPassThru = function (url, passThruList) {
        if ((passThruList) && (passThruList.length)) {
            return passThruList.every(function (pass) {
                return (new RegExp(pass)).test(url);
            });
        }
        else {
            return false;
        }
    };
    return UrlTester;
}());
UrlTester.crawlerUserAgents = [
    // 'googlebot',
    'yahoo',
    'bingbot',
    'baiduspider',
    'facebookexternalhit',
    'twitterbot',
    'rogerbot',
    'linkedinbot',
    'embedly',
    'quora link preview',
    'showyoubot',
    'outbrain',
    'pinterest/0.',
    'developers.google.com/+/web/snippet',
    'slackbot',
    'vkShare',
    'W3C_Validator',
    'redditbot',
    'Applebot',
    'WhatsApp',
    'flipboard',
    'tumblr',
    'bitlybot',
    'SkypeUriPreview',
    'nuzzel',
    'Discordbot',
    'Google Page Speed'
];
UrlTester.extensionsToIgnore = [
    '.js',
    '.map',
    '.css',
    '.xml',
    '.less',
    '.png',
    '.jpg',
    '.jpeg',
    '.gif',
    '.pdf',
    '.doc',
    '.txt',
    '.ico',
    '.rss',
    '.zip',
    '.mp3',
    '.rar',
    '.exe',
    '.wmv',
    '.doc',
    '.avi',
    '.ppt',
    '.mpg',
    '.mpeg',
    '.tif',
    '.wav',
    '.mov',
    '.psd',
    '.ai',
    '.xls',
    '.mp4',
    '.m4a',
    '.swf',
    '.dat',
    '.dmg',
    '.iso',
    '.flv',
    '.m4v',
    '.torrent',
    '.woff',
    '.ttf',
    '.svg'
];
exports.UrlTester = UrlTester;


/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("aws-sdk");

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

var AJAXTools = __webpack_require__(1);

module.exports = function(options) {

  return function(req, res, next) {

    if (!AJAXTools.AJAXTools.shouldServePrerenderPage(req, options)) {
      next(); //Shouldn't serve prerendered page, next to serve up the real page
    }
    else {
      AJAXTools.AJAXTools.getPrerenderPage(req, res, options)
        .then( function (pagebody) {
          res.set('Content-Type', 'text/html');
          res.send(pagebody);
        })
        .catch( function(err) {
          next();  //We failed, next to serve up the real page.
        });
    }
  }
};

/***/ })
/******/ ]);
});
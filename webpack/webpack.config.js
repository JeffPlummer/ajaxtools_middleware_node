var path = require('path');
var webpack = require('webpack');
var nodeExternals = require('webpack-node-externals');
var UnminifiedWebpackPlugin = require('unminified-webpack-plugin');

module.exports = {
  entry: './middleware.js',
  output: {
    filename: 'ajaxtools-express.min.js',
    path: path.resolve('./', 'dist'),
    library: 'ajaxtools-express',
    libraryTarget: 'umd'
  },
  target: 'node',
  externals: [nodeExternals()],
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new UnminifiedWebpackPlugin()
  ]
};